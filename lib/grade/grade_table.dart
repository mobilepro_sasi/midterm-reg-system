import 'package:flutter/material.dart';
import 'grade.dart';
import 'grade_cell.dart';

class GradeTable extends StatelessWidget {
  const GradeTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(constraints: const BoxConstraints(maxWidth: 500.0),
      child: Table(
        columnWidths: const <int, TableColumnWidth> {
          0:FixedColumnWidth(170) // 2 = index of column (count from 0)
        },
        defaultColumnWidth: const FlexColumnWidth(), // flex column อื่นที่ไม่ได้ fix ตามพื้นที่ที่เหลือ
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        children: <TableRow>[
          TableRow(
            decoration: BoxDecoration(
              color: Colors.limeAccent[100],
            ),
            children: const <Widget>[
              GradeCell(title: 'ผลการศีกษา', isHeader: true),
              GradeCell(title: '', isHeader: true),
            ],
          ),
          ...Grade.getGrades().map((grade) {
            return TableRow(children: [
              GradeCell(title: grade.type, isHeader: true),
              GradeCell(title: grade.info),
            ]);
          }),
        ],
      ),
    );

  }
}
