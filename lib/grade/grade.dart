class Grade {
  final String type;
  final String info;

  Grade(this.type, this.info);

  static List<Grade> getGrades() {
    return [
      Grade('คะแนนภาษาอังกฤษครั้งที่ 1:', '36 คะแนน'),
      Grade('คะแนนภาษาอังกฤษครั้งที่ 2:', '53 คะแนน'),
      Grade('หน่วยกิตคำนวณ:', '91 หน่วยกิต'),
      Grade('หน่วยกิตที่ผ่าน:', '91 หน่วยกิต'),
      Grade('คะแนนเฉลี่ยสะสม:', '3.23')
    ];
  }
}
