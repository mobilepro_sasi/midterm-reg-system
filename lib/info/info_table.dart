import 'package:flutter/material.dart';
import 'info.dart';
import 'info_cell.dart';

class InfoTable extends StatelessWidget {
  const InfoTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(constraints: const BoxConstraints(maxWidth: 500.0),
      child: Table(
        columnWidths: const <int, TableColumnWidth> {
          0:FixedColumnWidth(100) // 2 = index of column (count from 0)
        },
        defaultColumnWidth: const FlexColumnWidth(), // flex column อื่นที่ไม่ได้ fix ตามพื้นที่ที่เหลือ
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        children: <TableRow>[
          TableRow(
            decoration: BoxDecoration(
              color: Colors.limeAccent[100],
            ),
            children: const <Widget>[
              InfoCell(title: 'ข้อมูลการศึกษา', isHeader: true),
              InfoCell(title: '', isHeader: true),
            ],
          ),
          ...Info.getInfos().map((info) {
            return TableRow(children: [
              InfoCell(title: info.type),
              InfoCell(title: info.info),
            ]);
          }),
        ],
      ),
    );

  }
}
