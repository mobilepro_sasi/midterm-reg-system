import 'package:flutter/material.dart';
import 'subject/subject_table.dart';

class SubjectPage extends StatelessWidget {
  const SubjectPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
    appBar:  AppBar(
      title: const Text("วิชาที่ลงทะเบียน", style: TextStyle(fontSize: 24),),
      backgroundColor: Colors.limeAccent.shade100,
    ),
    body: profile(),
  );
}

Widget profile() {
  return Container(
    padding: const EdgeInsets.all(24),
    child: Center(
      child: ListView(
        children: [
          Text('ปีการศึกษา 2565'),
          Text('ภาคเรียนที่ 2'),
          Subject(),
        ],
      ),
    )
  );
}

Widget Subject() {
  return Center(
    child: SubjectTable(),
  );
}