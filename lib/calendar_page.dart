import 'package:flutter/material.dart';
import 'calendar/calendar_table.dart';

class CalendarPage extends StatelessWidget {
  const CalendarPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
    appBar:  AppBar(
      title: const Text("ปฏิทินการศึกษา", style: TextStyle(fontSize: 24),),
      backgroundColor: Colors.limeAccent.shade100,
    ),
    body: profile(),
  );
}

Widget profile() {
  return Container(
    padding: const EdgeInsets.all(24),
    child: Center(
      child: ListView(
        children: [
          Text('ปีการศึกษา 2565'),
          Text('ภาคเรียนที่ 2'),
          calendar(),
        ],
      ),
    )
  );
}

Widget calendar() {
  return Center(
    child: CalendarTable(),
  );
}