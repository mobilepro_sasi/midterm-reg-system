import 'package:flutter/material.dart';
import 'announce.dart';
import 'announce_cell.dart';

class AnnounceTable extends StatelessWidget {
  const AnnounceTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(constraints: const BoxConstraints(maxWidth: 500.0),
      child: Table(
        columnWidths: const <int, TableColumnWidth> {
          0:FixedColumnWidth(70) // 2 = index of column (count from 0)
        },
        defaultColumnWidth: const FlexColumnWidth(), // flex column อื่นที่ไม่ได้ fix ตามพื้นที่ที่เหลือ
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        children: <TableRow>[
          TableRow(
            decoration: BoxDecoration(
              color: Colors.limeAccent[100],
            ),
            children: const <Widget>[
              AnnouceCell(title: 'ประกาศ', isHeader: true),
              AnnouceCell(title: '', isHeader: true),
            ],
          ),
          ...Annouce.getAnnouces().map((annouce) {
            return TableRow(children: [
              AnnouceCell(title: annouce.no, isHeader: true),
              AnnouceCell(title: annouce.annouce),
            ]);
          }),
        ],
      ),
    );

  }
}
