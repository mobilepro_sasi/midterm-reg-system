import 'package:flutter/material.dart';
import 'announce.dart';

class AnnouceCell extends StatelessWidget {
  final String title;
  final bool isHeader;

  const AnnouceCell({
    Key? key,
    required this.title,
    this.isHeader = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: isHeader ? Alignment.center : Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Text(title,
            style: TextStyle(
                fontWeight: isHeader ? FontWeight.bold : FontWeight.normal,),),
      ),
    );
  }
}
