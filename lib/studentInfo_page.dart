import 'package:flutter/material.dart';
import 'info/info_table.dart';

class studentInfoPage extends StatelessWidget {
  const studentInfoPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
    appBar:  AppBar(
      title: const Text("ประวัตินิสิต", style: TextStyle(fontSize: 24),),
      backgroundColor: Colors.limeAccent.shade100,
    ),
    body: profile(),
  );
}

Widget profile() {
  return Container(
    padding: const EdgeInsets.all(24),
    child: Center(
      child: Column(
        children: [
          CircleAvatar(
            radius: 52,
            backgroundImage: NetworkImage('https://m.media-amazon.com/images/I/31Cd9UQp6eL._AC_SY780_.jpg'
            ),
          ),
          SizedBox(height: 12,),
          Text('63160287',
            style: TextStyle(fontSize: 24),),
          Text('ศศินา มู้เม่า',
            style: TextStyle(fontSize: 20),),
          infoTable(),
        ],
      ),
    )
  );
}

Widget infoTable() {
  return Center(child: InfoTable(),);
}