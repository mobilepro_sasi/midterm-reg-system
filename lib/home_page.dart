import 'package:flutter/material.dart';
import 'grade/grade_table.dart';
import 'annoucement/announce_table.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
    body: Main(),
  );
}

Widget Main() {
  return Container(
      padding: const EdgeInsets.all(24),
      child: Center(
        child: Column(
          children: [
            SizedBox(height: 12,),
            Grade(),
            SizedBox(height: 12,),
            Annouce(),
          ],
        ),
      )
  );
}

Widget Grade() {
  return Container(
    child: Center(
      child: GradeTable(),
    ),
  );
}

Widget Annouce() {
  return Container(
    child: Center(
      child: AnnounceTable(),
    ),
  );
}