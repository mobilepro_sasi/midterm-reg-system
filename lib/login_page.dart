import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.limeAccent[100],
          title: const Text(
            "เข้าสู่ระบบ",
            style: TextStyle(
                fontWeight: FontWeight.w600, fontSize: 25, letterSpacing: 1.5),
          ),
        ),
        body: Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 300.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const Spacer(
                  flex: 5,
                ),
                const Spacer(
                  flex: 5,
                ),
                const Text(
                  'มหาวิทยาลัยบูรพา',
                  style: TextStyle(fontSize: 26, fontWeight: FontWeight.w500),
                ),
                const Spacer(
                  flex: 5,
                ),
                const TextField(
                    decoration: InputDecoration(labelText: 'รหัสประจำตัว')),
                const TextField(
                    decoration: InputDecoration(labelText: 'รหัสผ่าน')),
                const Spacer(flex: 3),
                ElevatedButton(
                  onPressed: () {
                    // Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const HomePage()));
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.limeAccent.shade100,
                    padding: const EdgeInsets.only(
                        left: 20, right: 20, top: 10, bottom: 10),
                  ),
                  child: const Text(
                    'เข้าสู่ระบบ',
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.w300),
                  ),
                ),
                const Spacer(
                  flex: 20,
                ),
              ],
            ),
          ),
        ));
  }
}
