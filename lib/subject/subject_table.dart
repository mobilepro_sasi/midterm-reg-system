import 'package:flutter/material.dart';
import 'subject.dart';
import 'subject_cell.dart';

class SubjectTable extends StatelessWidget {
  const SubjectTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(constraints: const BoxConstraints(maxWidth: 500.0),
      child: Table(
        columnWidths: const <int, TableColumnWidth> {
          0:FixedColumnWidth(100),
          2:FixedColumnWidth(40)// 2 = index of column (count from 0)
        },
        defaultColumnWidth: const FlexColumnWidth(), // flex column อื่นที่ไม่ได้ fix ตามพื้นที่ที่เหลือ
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        children: <TableRow>[
          TableRow(
            decoration: BoxDecoration(
              color: Colors.limeAccent[100],
            ),
            children: const <Widget>[
              SubjectCell(title: 'รหัสวิชา', isHeader: true),
              SubjectCell(title: 'ชื่อรายวิชา', isHeader: true),
              SubjectCell(title: 'กลุ่ม', isHeader: true),
            ],
          ),
          ...Subject.getSubjects().map((subject) {
            return TableRow(children: [
              SubjectCell(title: subject.no),
              SubjectCell(title: subject.subject),
              SubjectCell(title: subject.sec),
            ]);
          }),
        ],
      ),
    );

  }
}
