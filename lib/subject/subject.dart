class Subject {
  final String no;
  final String subject;
  final String sec;

  Subject(this.no, this.subject,this.sec);

  static List<Subject> getSubjects() {
    return [
      Subject('79322263-63', 'Cosmetic and Beauty เครื่องสำอางกับความงาม', '1'),
      Subject('88624359-59', 'Web Programming การเขียนโปรแกรมบนเว็บ', '2'),
      Subject('88624459-59', 'Object-Oriented Analysis and Design การวิเคราะห์และออกแบบเชิงวัตถุ', '2'),
      Subject('88624559-59', 'Software Testing การทดสอบซอฟต์แวร์', '2'),
      Subject('88634259-59', 'Multimedia Programming for Multiplatforms การโปรแกรมสื่อผสมสำหรับหลายแพลตฟอร์ม', '2'),
      Subject('88634459-59', 'Mobile Application Development I การพัฒนาโปรแกรมประยุกต์บนอุปกรณ์เคลื่อนที่ 1', '2'),
      Subject('88646259-59', 'Introduction to Natural Language Processing การประมวลผลภาษาธรรมชาติเบื้องต้น', '2'),
    ];
  }
}
