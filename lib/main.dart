import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'package:midterm_reg/studentInfo_page.dart';
import 'home_page.dart';
import 'package:midterm_reg/login_page.dart';
import 'package:midterm_reg/calendar_page.dart';
import 'package:midterm_reg/subject_page.dart';

void main() {
  runApp(RegSystem());
}

class RegSystem extends StatefulWidget {
  @override
  State<RegSystem> createState() => _RegSystem();
}

class _RegSystem extends State<RegSystem> {
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Reg System',
        theme: ThemeData(
          primarySwatch: Colors.grey,
        ),
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: const Text('มหาวิทยาลัยบูรพา', style: TextStyle(fontSize: 24),),
            backgroundColor: Colors.limeAccent.shade100,
          ),
          drawer: NavigationDrawer(),
          body: HomePage(),
        ),
      ),
    );
  }
}

class NavigationDrawer extends StatefulWidget {
  State<NavigationDrawer> createState() => _NavigationDrawer();
}

class _NavigationDrawer extends State<NavigationDrawer> {
  // const NavigationDrawer({Key? key}) : super(key: key)

  @override
  Widget build(BuildContext context) => Drawer(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              buildHeader(context),
              buildMenuItems(context),
            ],
          ),
        ),
      );

  Widget buildHeader(BuildContext context) => Container(
    color: Colors.limeAccent.shade100,
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).padding.top,
        ),
    child: Column(
      children: [
        CircleAvatar(
          radius: 52,
          backgroundImage: NetworkImage('https://m.media-amazon.com/images/I/31Cd9UQp6eL._AC_SY780_.jpg'
          ),
        ),
        SizedBox(height: 12,),
        Text('63160287',
        style: TextStyle(fontSize: 24),),
        Text('ศศินา มู้เม่า',
          style: TextStyle(fontSize: 20),)
      ],
    ),
      );

  Widget buildMenuItems(BuildContext context) => Container(
        padding: const EdgeInsets.all(24),
        child: Wrap(
          runSpacing: 16,
          children: [
            ListTile(
              leading: const Icon(Icons.menu_rounded),
              title: const Text("หน้าหลัก", style: TextStyle(fontSize: 18),),
              onTap: () {},
            ),
            ListTile(
              leading: const Icon(Icons.info_outline_rounded),
              title: const Text("ประวัตินิสิต" , style: TextStyle(fontSize: 18),),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => const studentInfoPage()));
              },
            ),
            ListTile(
              leading: const Icon(Icons.table_chart_outlined),
              title: const Text("วิชาที่ลงทะเบียนเรืยน", style: TextStyle(fontSize: 18),),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => const SubjectPage()));
              },
            ),
            ListTile(
              leading: const Icon(Icons.calendar_month),
              title: const Text("ปฏิทินการศึกษา", style: TextStyle(fontSize: 18),),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => const CalendarPage()));
              },
            ),
            const Divider(
              color: Colors.black54,
            ),
            ListTile(
              leading: const Icon(Icons.logout),
              title: const Text("ออกจากระบบ", style: TextStyle(fontSize: 18),),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => const LoginPage()));
              },
            ),
          ],
        ),
      );
}
