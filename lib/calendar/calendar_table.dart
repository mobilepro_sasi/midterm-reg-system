import 'package:flutter/material.dart';
import 'calendar.dart';
import 'calendar_cell.dart';

class CalendarTable extends StatelessWidget {
  const CalendarTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(constraints: const BoxConstraints(maxWidth: 500.0),
      child: Table(
        columnWidths: const <int, TableColumnWidth> {
          0:FixedColumnWidth(200) // 2 = index of column (count from 0)
        },
        defaultColumnWidth: const FlexColumnWidth(), // flex column อื่นที่ไม่ได้ fix ตามพื้นที่ที่เหลือ
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        children: <TableRow>[
          TableRow(
            decoration: BoxDecoration(
              color: Colors.limeAccent[100],
            ),
            children: const <Widget>[
              CalendarCell(title: 'รายการ', isHeader: true),
              CalendarCell(title: 'วันเริ่มต้น', isHeader: true),
              CalendarCell(title: 'วันสิ้นสุด', isHeader: true),
            ],
          ),
          ...Calendar.getCalendar().map((calendar) {
            return TableRow(children: [
              CalendarCell(title: calendar.list),
              CalendarCell(title: calendar.start),
              CalendarCell(title: calendar.end),
            ]);
          }),
        ],
      ),
    );

  }
}
